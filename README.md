1. Run service
2. send post request with html to `/convert` endpoint example 
```
curl -X POST 'http://localhost:8090/convert'   -F 'html="<!DOCTYPE html>
<html>
<body>                             
<h1>My First Heading</h1>
<p>My first paragraph.</p>
</body>
</html>
"' --output response.pdf
```
3. endpoint returns binary data, write data to the file