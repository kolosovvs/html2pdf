package main;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


class Converter {
    Path convert(String html) throws IOException {
        Path pathToTempPdfFile = Files.createTempFile("converter", ".pdf");
        ConverterProperties converterProperties = new ConverterProperties();
        FileOutputStream stream = new FileOutputStream(pathToTempPdfFile.toString());
        HtmlConverter.convertToPdf(html, stream, converterProperties);
        return pathToTempPdfFile;
    }
}
