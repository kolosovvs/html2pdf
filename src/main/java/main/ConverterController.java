package main;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


@RestController
public class ConverterController {

    @RequestMapping(value ="/convert", method=RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<byte[]> convert(@RequestParam(required = true) String html) throws IOException {
        Converter converter = new Converter();
        Path pathToTempPdfFile = converter.convert(html);
        String path = pathToTempPdfFile.toString();
        String fileName = path.split("/")[2].replace(" ", "_");
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+fileName);
        return new ResponseEntity<>(Files.readAllBytes(pathToTempPdfFile), headers, HttpStatus.OK);
    }
}